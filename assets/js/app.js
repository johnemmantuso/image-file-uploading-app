(function ($) {
    'use strict';

    function openModal() {
        $('#uploadModal').fadeIn();
    }

    function closeModal() {
        $('#uploadModal').fadeOut();
    }

    function fadeMessage() {
        setTimeout(() => {
            $('#notificationWrapper').children().fadeOut('slow', function () {
                $('#notificationWrapper').children().remove();
            });
        }, 3000);
    }

    function showMessage(message, status, el) {
        var messageTemplate =
            '<p class="message message--' + status + '">' +
            message +
            '</p>';
        $(el).append($(messageTemplate));
    }

    function getImage($id){
        var request = $.ajax({
            url: siteUrl + 'index.php/app/get_image',
            method: "POST",
            data: {
                'image_id':$id,
            },
            dataType: 'json',
        });

        request.done(function (data) {
            var imageRow = '';
            if (data) {
                data.forEach(image => {
                    imageRow += '<tr data-image-id="' + image['id'] + '">';
                    imageRow += '<td>' + image['title'] + '</td>';
                    imageRow += '<td><img src="' + siteUrl + 'uploads/' +image['thumbnail'] + '" alt="' + image['title'] + '" /></td>';
                    imageRow += '<td>' + image['filename'] + '</td>';
                    imageRow += '<td>' + image['date_added'] + '</td>';
                    imageRow += '<td>';
                    imageRow += '<button class="btn btn--success js-image-edit" type="button" data-image-id="' + image['id'] + '">';
                    imageRow += '<svg class="icon icon-pencil"><use xlink:href="#icon-pencil"></use></svg>';
                    imageRow += '</button>';
                    imageRow += '<button class="btn js-image-delete" type="button" data-image-id="' + image['id'] + '">';
                    imageRow += '<svg class="icon icon-bin"><use xlink:href="#icon-bin"></use></svg>';
                    imageRow += '</button>';    
                    imageRow += '</td>';
                    imageRow += '</tr>';
                });
                $('#imageTableBody').append($(imageRow));
            }
        });

        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    }

    function deleteImage($id){
        var request = $.ajax({
            url: siteUrl + 'index.php/app/delete_image',
            method: "POST",
            data: {
                'image_id':$id,
            },
            dataType: 'json',
        });

        request.done(function (data) {
            if (data) {
                $('#imageTableBody').find('tr[data-image-id="' + $id + '"]').fadeOut(function(){
                    $(this).remove();
                });
            }
        });

        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    }

    $(function () {
        $('#uploadBtn').on('click', function () {
            openModal();
        });

        $('#closeModal').on('click', function () {
            closeModal();
        });

        $('#uploadForm').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            var formData = new FormData(form[0]);

            var request = $.ajax({
                url: url,
                method: "POST",
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
            });

            request.done(function (data) {
                if (data.success.trim() === 'success') {
                    showMessage(data.message, 'success', '#notificationWrapper');
                    getImage(data.image_id);
                } else {
                    showMessage(data.message, 'error', '#notificationWrapper');
                }
                fadeMessage();
                closeModal();
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        });

        $(document).on('click', '.js-image-delete', function(){
            var imageId = $(this).attr('data-image-id');
            deleteImage(imageId);
        });
    });
})(jQuery);