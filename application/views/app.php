<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        <?php echo $title; ?>
    </title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo URL::base(); ?>assets/css/screen.css">
</head>

<body>
    <main class="app-content">
        <button class="btn btn--primary btn--lg" id="uploadBtn" type="button">
            <svg class="icon icon-upload"><use xlink:href="#icon-upload"></use></svg>
            Upload Image
        </button>

        <div id="notificationWrapper" class="notification-wrapper"></div>

        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Thumbnail</th>
                    <th>Filename</th>
                    <th>Date added</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody id="imageTableBody">
                <?php foreach ( $images as $image ) :
                    $image_thumbnail = $image['thumbnail'];
                    $image_thumbnail = URL::site("/uploads/$image_thumbnail", TRUE);
                    $image_thumbnail = str_replace('index.php/', '', $image_thumbnail);
                ?>
                <tr data-image-id="<?php echo $image['id']; ?>">
                    <td><?php echo $image['title']; ?></td>
                    <td><img src="<?php echo $image_thumbnail ?>" alt="Uploaded avatar" /></td>
                    <td><?php echo $image['filename']; ?></td>
                    <td><?php echo $image['date_added']; ?></td>
                    <td>
                        <button class="btn js-image-edit btn--success" data-image-id="<?php echo $image['id']; ?>" type="button"><svg class="icon icon-pencil"><use xlink:href="#icon-pencil"></use></svg></button>
                        <button class="btn js-image-delete" data-image-id="<?php echo $image['id']; ?>" type="button"><svg class="icon icon-bin"><use xlink:href="#icon-bin"></use></svg></button>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </main>

    <div class="modal-backdrop" id="uploadModal">
        <div class="modal">
            <form id="uploadForm" action="<?php echo URL::site('app/upload', TRUE);?>" method="post" enctype="multipart/form-data">
                <header class="modal__header">
                    <h2 class="modal__title">Select File To Upload</h2>
                </header>
                <div class="modal__body">

                    <div class="form-group">
                        <label for="imageTitle">Title</label>
                        <input type="text" class="form-control" name="image_title" id="image_title" placeholder="Image Title">
                    </div>
                    <div class="form-group">
                        <label for="imageFile">Image</label>
                        <input type="file" name="image_file" id="image_file">
                    </div>
                </div>
                <footer class="modal__footer">
                    <button class="btn btn--success" type="submit">
                        <svg class="icon icon-upload">
                            <use xlink:href="#icon-upload"></use>
                        </svg>
                        Upload
                    </button>
                    <button class="btn" type="button" id="closeModal">
                        <svg class="icon icon-cross">
                            <use xlink:href="#icon-cross"></use>
                        </svg>
                        Cancel
                    </button>
                </footer>
            </form>
        </div>
    </div>
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-pencil" viewBox="0 0 32 32">
                <title>Pencil</title>
                <path d="M27 0c2.761 0 5 2.239 5 5 0 1.126-0.372 2.164-1 3l-2 2-7-7 2-2c0.836-0.628 1.874-1 3-1zM2 23l-2 9 9-2 18.5-18.5-7-7-18.5 18.5zM22.362 11.362l-14 14-1.724-1.724 14-14 1.724 1.724z"></path>
            </symbol>
            <symbol id="icon-upload" viewBox="0 0 32 32">
                <title>Upload</title>
                <path d="M14 18h4v-8h6l-8-8-8 8h6zM20 13.5v3.085l9.158 3.415-13.158 4.907-13.158-4.907 9.158-3.415v-3.085l-12 4.5v8l16 6 16-6v-8z"></path>
            </symbol>
            <symbol id="icon-bin" viewBox="0 0 32 32">
                <title>Bin</title>
                <path d="M6 32h20l2-22h-24zM20 4v-4h-8v4h-10v6l2-2h24l2 2v-6h-10zM18 4h-4v-2h4v2z"></path>
            </symbol>
            <symbol id="icon-cross" viewBox="0 0 32 32">
                <title>cross</title>
                <path d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z"></path>
            </symbol>
        </defs>
    </svg>
    <script src="<?php echo URL::base(); ?>assets/js/lib/jquery-3.3.1.min.js"></script>
    <script>
        var siteUrl = '<?php echo URL::base(TRUE); ?>';
    </script>
    <script src="<?php echo URL::base(); ?>assets/js/app.js"></script>
</body>

</html>
