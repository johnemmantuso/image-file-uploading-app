<?php defined('SYSPATH') or die('No direct script access.');
 
class Controller_App extends Controller {
 
    public function action_index()
    {
        $view = View::factory('app');
        $view->set('title', 'Image File Uploading App');

        $app_model = Model::factory('app');
        
        $view->images = $app_model->get_all_images();
        $this->response->body($view);
    }
 
    public function action_upload()
    {
        $message = NULL;
        $filename = NULL;
        $status = NULL;
        $image_id = NULL;
        $image_title = NULL;
 
        if ($this->request->method() == Request::POST)
        {
            if (isset($_FILES['image_file'])) {
                $filename = $this->_save_image($_FILES['image_file']);
            }
            if (isset($_POST['image_title']) && ! empty($_POST['image_title'])) {
                $image_title = $_POST['image_title'];
            } else {
                $image_title = 'NO TITLE';
            }
        }
        
        // Image is upload add details to database.
        if (! $filename) {
            $message = 'There was a problem while uploading the image.
                Make sure it is a JPG/PNG/GIF file.';
                $status = 'error';
        } else {
            $message = 'Image is successfulyl uploaded!';
            $status = 'success';
            $image_id = $this->_add_image_db($image_title, $filename);
            if ( is_array($image_id) ) {
                $image_id = $image_id[0];
            }
        }

        $this->response->body(json_encode(
            array(
                'success'  => $status,
                'fileName' => $filename,
                'message'  => $message,
                'image_id' => $image_id,
            )
        ));
    }

    protected function _add_image_db($title, $filename)
    {
        $app_model = Model::factory('app');

        return $app_model->add_image(array(
            'title' => $title,
            'image_thumbnail' => $filename.'-thumb.jpg',
            'image_filename' => $filename,
            'date_added' => date("Y-m-d")
        ));
    }
    
    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'uploads/';

        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20));
 
            Image::factory($file)
                ->resize(100, 100, Image::AUTO)
                ->save($directory.$filename.'-thumb.jpg');

            Image::factory($file)
                ->save($directory.$filename.'.jpg');
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }

    public function action_get_image()
    {
        $app_model = Model::factory('app');
        $image = null;
        if ($this->request->method() == Request::POST)
        {
            if (isset($_POST['image_id']))
            {
                $image = $app_model->get_image($_POST['image_id']);
            }

        }
        $this->response->body(json_encode(
            $image
        ));
    }

    public function action_delete_image()
    {
        $app_model = Model::factory('app');
        $image = null;
        if ($this->request->method() == Request::POST)
        {
            if (isset($_POST['image_id']))
            {
                $image = $app_model->delete_image($_POST['image_id']);
            }
        }
        $this->response->body(json_encode(
            $image
        ));
    }
}