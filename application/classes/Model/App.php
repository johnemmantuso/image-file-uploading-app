<?php defined('SYSPATH') or die('No direct script access.');

Class Model_App extends Model_Database
{
    public function get_image($image_id) {
        $sql = 'SELECT * FROM `images` WHERE id = ' . $image_id;
        return $this->_db->query(Database::SELECT, $sql, FALSE)
                         ->as_array();
    }

    public function get_all_images() {
        $sql = 'SELECT * FROM `images`';
        return $this->_db->query(Database::SELECT, $sql, FALSE)
                         ->as_array();
    }

    public function delete_image($image_id)
    {
        return DB::delete('images')->where('id', 'IN', array($image_id))->execute();
    }

    public function add_image($d)
    {
        $insert_id = DB::insert(
            'images',
            array(
                'title',
                'thumbnail',
                'filename',
                'date_added'
                )
            )->values(
                array(
                    $d['title'],
                    $d['image_thumbnail'],
                    $d['image_filename'],
                    $d['date_added']
                )
            )
            ->execute();
        return $insert_id;
    }
}
